using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueOrbAbility : MonoBehaviour, IAbility
{
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private float projectileSpeed = 50f;

    public void Awake()
    {
        projectilePrefab = Resources.Load("Prefabs/Projectiles/Projectile") as GameObject;
    }

    public void UseAbility(Transform userTransform)
    {
        GameObject go = Instantiate
            (projectilePrefab, userTransform.position,userTransform.rotation);

        Rigidbody rigid = go.GetComponent<Rigidbody>();
        rigid.AddForce(userTransform.forward * projectileSpeed, ForceMode.VelocityChange);
    }
}


