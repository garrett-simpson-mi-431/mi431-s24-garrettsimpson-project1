using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BlueOrbPickup : Pickup
{
    public override void UsePickup(Player player)
    {
        //Equip BlueOrb ability to the player
        player.EquippedAbility = player.gameObject.AddComponent<BlueOrbAbility>();
        Debug.Log(player.EquippedAbility);
        StartCoroutine(DestroyPickup());
    }

    public override IEnumerator DestroyPickup()
    {
        GetComponent<SphereCollider>().enabled = false;
        vfx.Stop();

        yield return new WaitUntil(() => vfx.aliveParticleCount == 0);
        Destroy(gameObject);
    }
}
