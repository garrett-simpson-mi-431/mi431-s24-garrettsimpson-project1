using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public abstract class Pickup : MonoBehaviour
{
    [SerializeField] protected VisualEffect vfx;

    public abstract void UsePickup(Player player);
    public abstract IEnumerator DestroyPickup();
}

