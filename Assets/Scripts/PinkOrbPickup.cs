using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkOrbPickup : Pickup
{
    public override void UsePickup(Player player)
    {
        player.speed *= 2;
        StartCoroutine(DestroyPickup());
    }

    public override IEnumerator DestroyPickup()
    {
        GetComponent<SphereCollider>().enabled = false;
        vfx.Stop();

        yield return new WaitUntil(() => vfx.aliveParticleCount == 0);
        Destroy(gameObject);
    }
}
