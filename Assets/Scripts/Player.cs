using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Rigidbody rigid;
    public float xSensitivity = 10;
    public float ySensitivity = 10;

    public float xRot;
    public float yRot;

    public float speed = 5;

    public Camera playerCamera;

    public IAbility EquippedAbility;

    
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        rigid = GetComponent<Rigidbody>();
        xRot = transform.eulerAngles.x;
        yRot = transform.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        HandleRotation();

        //If player has an ability and right clicks, use ability
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (EquippedAbility != null) EquippedAbility.UseAbility(playerCamera.transform);
        }
    }

    void FixedUpdate()
    {
        HandleMovement();
    }

    //Handles Player and Camera rotation
    void HandleRotation()
    {
        float mouseX = Input.GetAxis("Mouse X") * xSensitivity;
        float mouseY = Input.GetAxis("Mouse Y") * ySensitivity;

        yRot += mouseX;
        xRot -= mouseY;

        xRot = Mathf.Clamp(xRot, -90, 90);

        transform.eulerAngles = new Vector3(0, yRot, 0);
        playerCamera.transform.eulerAngles = new Vector3(xRot, transform.eulerAngles.y, transform.eulerAngles.z);
    }

    //Handles player movement
    void HandleMovement()
    {
        
        float moveSide = Input.GetAxis("Horizontal");
        float moveForward = Input.GetAxis("Vertical");

        Vector3 moveDirection = (transform.forward * moveForward) + (transform.right * moveSide);

        rigid.velocity = moveDirection * speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        Pickup PUP = other.GetComponent<Pickup>();
        if (PUP != null) PUP.UsePickup(this);
    }
}
