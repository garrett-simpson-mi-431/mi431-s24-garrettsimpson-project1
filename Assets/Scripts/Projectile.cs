using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class Projectile : MonoBehaviour
{
    public int projectileLifetime = 5;
    public VisualEffect projVFX;

    private void Awake()
    {
        StartCoroutine(KillInSeconds(projectileLifetime));
    }

    //Projectile will be destroyed after the amount of seconds
    private IEnumerator KillInSeconds(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        StartCoroutine(DestroyProjectile());
    }

    private void OnCollisionEnter(Collision collision)
    {
        //currently if this hits anything, it is destroyed without anything else happening
        GetComponent<Rigidbody>().velocity = Vector3.zero;
        StartCoroutine(DestroyProjectile());
    }

    public IEnumerator DestroyProjectile()
    {
        GetComponent<SphereCollider>().enabled = false;
        projVFX.SetBool("conformToSphere", false);
        projVFX.Stop();

        yield return new WaitForSeconds(3);
        Destroy(gameObject);
    }
}
